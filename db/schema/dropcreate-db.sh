#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

    source "$AH/.env"

        [ -z $DB_HOST ]           && (echo "ERROR Envvar DB_HOST not found"; kill $$)
        [ -z $DB_CONTAINER_NAME ] && (echo "ERROR Envvar DB_HOST not found"; kill $$)

        # make db blank
        docker exec -t $DB_CONTAINER_NAME bash -c "psql postgresql://$DB_USER:$DB_PASS@/postgres -c 'DROP DATABASE if exists $DB_NAME'"
        docker exec -t $DB_CONTAINER_NAME bash -c "psql postgresql://$DB_USER:$DB_PASS@/postgres -c 'CREATE DATABASE $DB_NAME'"

        # create db for test
        DB_TEST=${DB_NAME}_test
        docker exec -t $DB_CONTAINER_NAME bash -c "psql postgresql://$DB_USER:$DB_PASS@/postgres -c 'DROP DATABASE if exists $DB_TEST'"
        docker exec -t $DB_CONTAINER_NAME bash -c "psql postgresql://$DB_USER:$DB_PASS@/postgres -c 'CREATE DATABASE $DB_TEST'"

        sleep 3  # give the db some time to warm up
        echo 'Done'
