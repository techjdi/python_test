#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

cd $SH
set -e  # halt if error ref. https://stackoverflow.com/a/3474556/248616
    ./dropcreate-db.sh
    ./sync/00.alembic-rev--2--db.sh
    ./show-table.sh
