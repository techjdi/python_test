#!/bin/bash
[ -z $r ] && (echo 'Envvar $r is required'; kill $$)

SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../../.." && pwd)

cd $AH
    PYTHONPATH=$AH  pipenv run  alembic stamp $r
