CAUTION that we all together must agree on this `rule on db schema update`
> DO NOT update db shema physically/directly
> any db changes MUST BE THRU updating sqlalchemy model first

re-create tables from scratch
```
./dropcreate-db.sh ; ./sync/00.alembic-rev--2--db.sh ; ./show-table.sh 
```

new changes from either db or model --> generate new alembic migrate
```
db/schema/sync/01.compare-db-vs-model--2--alembic.sh
``` 
