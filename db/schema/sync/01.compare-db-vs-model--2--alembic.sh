#!/bin/bash
docstring="
m='add table users'                                 db/schema/model-to-db.sh
m='add table users' vp='db/alembic/versions/202105' db/schema/model-to-db.sh
"

[ -z $m ]   && m='new_revision'
[ -z $vp ]  && vp='db/alembic/versions/202105'

SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../../.." && pwd)

cd $AH
    PYTHONPATH=$AH  pipenv run  alembic revision --autogenerate  -m "$m"  --version-path "$vp"
