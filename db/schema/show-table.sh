#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

    source "$AH/.env"

        [ -z $DB_HOST ]           && (echo "ERROR Envvar DB_HOST not found"; kill $$)
        [ -z $DB_CONTAINER_NAME ] && (echo "ERROR Envvar DB_HOST not found"; kill $$)

        # make db blank
        docker exec -t $DB_CONTAINER_NAME bash -c "psql postgresql://$DB_USER:$DB_PASS@/$DB_NAME  -P pager=off  -c \"SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' \" "
        #                                                                                         no pager          list table names
        #                                                                                                           ref. https://stackoverflow.com/a/15644435/248616
        #                                                                                         ref. https://stackoverflow.com/a/15621397/248616
