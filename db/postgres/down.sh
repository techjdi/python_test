#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

set -a  # broadcast defined var as envvar to child sub routine ON
    source "$AH/.env"  # load app envvar  eg db DB_xx port/user/pass/name to connect to
    [ $? != 0 ] && (echo  "ERROR cannot load $AH/.env"; kill $$)
        [ -z $RR_CONTAINER_PROJECT ] && (echo "RR_CONTAINER_PROJECT is required"; kill $$)

        cd $SH
            set -x  # autoprint command when run
            docker-compose  -p $RR_CONTAINER_PROJECT  -f "$SH/docker-compose.yml"  down -t0
