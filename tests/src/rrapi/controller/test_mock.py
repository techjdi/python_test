from falcon.testing import TestCase
import falcon

from src.rrapi.app import app
from src.rrapi.controller.mock import mock_u__ALL, mock_u__Oliver, mock_u__Ryan
from src.rrapi.controller.mock import mock_m__ALL, mock_m__Oliver


class TestAPI(TestCase):  # ref. https://gist.github.com/kgriffs/00479d0bf94db94a9bfc6745535bce01

    def setUp(self):
        super(TestAPI, self).setUp()

        self.app = app


    def test_0th(self):
        r = self.simulate_get('/mocks')  # https://stackoverflow.com/a/43278734/248616
        assert r.status != falcon.HTTP_200


    def test_users_GET(self):
        r = self.simulate_get('/mocks?n=users&m=GET')  # https://stackoverflow.com/a/43278734/248616
        assert r.status == falcon.HTTP_200
        assert r.json == mock_u__ALL


    def test_users_GET_atid_1(self):
        r = self.simulate_get('/mocks?n=users/1&m=GET')  # https://stackoverflow.com/a/43278734/248616
        assert r.status == falcon.HTTP_200
        assert r.json == mock_u__Oliver


    def test_users_GET_atid_2(self):
        r = self.simulate_get('/mocks?n=users/2&m=GET')
        assert r.status == falcon.HTTP_200
        assert r.json == mock_u__Ryan


    def test_matchings_GET(self):
        r = self.simulate_get('/mocks?n=matchings&m=GET')  # https://stackoverflow.com/a/43278734/248616
        assert r.status == falcon.HTTP_200
        assert r.json == mock_m__ALL


    def test_matchings_GET_atid_1(self):
        r = self.simulate_get('/mocks?n=matchings/1&m=GET')  # https://stackoverflow.com/a/43278734/248616
        assert r.status == falcon.HTTP_200
        assert r.json == mock_m__Oliver
