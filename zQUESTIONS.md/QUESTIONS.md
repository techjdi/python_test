## THE PROBLEM
Given that your access to this git repo gitlab.com/namgivu/referreach_python_tech.git, 
please proceed the below requirement(s).

Follow the README.md and

00 Set up .env to edit the configuration <br>
01 Get postgres database up running <br>
02 Succeed running unittest under tests/ folder <br>

03a Understand how endpoint /mocks is serving json result to various scenario - they were created to have the json matching with what we can see in the UI-mock screens below.

03b Implement the mock endpoint(s) users/ into real working one <br>
  1 Create new git branch from main branch, and name it with candidate/yourname <br>
  2 Discuss your database schema changes with techjdi team via slack to get approval to proceed <br>
  3 Implement schema changes as sqlalchemy model under src/rrapi/models/ <br>
  4 Implement api endpoint so that it return the json as much similar as possible to the according mock endpoint <br> 
  5 Assert the endpoint output is as expected by unittesting with pytest - please refer to sample pytest code at [tests/src/rrapi/controller/test_mock.py](../tests/src/rrapi/controller/test_mock.py) <br>

03c (optional) Implement the mock endpoint(s) matchings/ into real working one; similar requirements as endpoint users/ above


## THE UI-MOCKUP SCREEN
Related screens for /users/1 endpoint

![](./i/210604.profile.png)


## real vs mock endpoint syntax
```
target endpoint    mock endpoint syntax
---------------    ---------------------- 
GET /users         /mocks?m=GET&n=users
GET /users/1       /mocks?m=GET&n=users/1
