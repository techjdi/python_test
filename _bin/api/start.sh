#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
AH=`cd "$SH/../.." && pwd`

cd $SH
    # PYTHONPATH=$AH  pipenv run gunicorn --help
    PYTHONPATH=$AH  pipenv run gunicorn --reload src.rrapi.app -b "0.0.0.0:20502"  #TODO make 20502 an envvar
