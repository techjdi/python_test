#!/bin/bash
pid_all=`ps aux | grep -E 'src.gunicorn.+rrapi' | grep -v 'grep' | awk '{print $2}'`  #TODO add port to stop more precisely

for pid in $pid_all; do
    echo -e "\nKilling process pid=$pid..."
    kill -9 $pid
done
