import falcon
import json


class MockResource:

    def on_get(self, req, resp):
        valid_request = handle_request(req, resp)
        if not valid_request: raise Exception('Unsupported request')


#region mock result vs endpoint mapping

#region endpoint /users

mock_u__Oliver = { # ref. https://www.figma.com/proto/Zj69CPa3UJnjhZdUcBRRhr/Refer-Reach?node-id=16%3A153&scaling=min-zoom
    'id'         : 1,
    'first_name' : 'Oliver',
    'last_name'  : 'Grayson',

    'my_self': {
        'industry'   : ['fashion', ],
        'biztype'    : 'B2B/B2C',
        'self_intro' : 'eg We provide venture support services, such as tech, ops, biz, etc. to help startups in series A stage and above grow into Vietnam market quickly',
    },
    'my_client': {
        'industry': ['fashion', 'textile', 'design', 'branding', 'technology', ],  #TODO option sell to all
    },
    'my_partner': {
        'industry': ['publishing', 'modeling', ],  #NOTE there's NO option for all here
    },

    # 'profile_photo': b'some base64 encoded data here',  #TODO get this working; error currently > TypeError: Object of type bytes is not JSON serializable

    'profile_complete' : .56,  # ref. https://www.figma.com/proto/Zj69CPa3UJnjhZdUcBRRhr/Refer-Reach?node-id=16%3A153&scaling=min-zoom
    'network_reach'    : 890,

    'group' : {  # ref. https://www.figma.com/proto/Zj69CPa3UJnjhZdUcBRRhr/Refer-Reach?node-id=95%3A737&scaling=min-zoom
        'joined'  : ['New York Fashion Club', 'Los Angeles Fashion Club', ],
        'invited' : [
            {'to_group': 'Style by SCARLET', 'by': 'Jacqueline Carlyle', },
        ],
        'pending_approval' : [
            {'to_group': 'Style by SCARLET', 'connection_count_in_group': 2, },
        ],
    },

}

mock_u__Ryan = {
    'id'         : 2,
    'first_name' : 'Ryan',
    'last_name'  : 'Decker',

    'my_self': {
        'industry' : ['book', ],
        'biztype'  : 'B2B/B2C',
    },
    'my_client': {
        'industry': ['social media', 'technology', ],
    },
    'my_partner': {
        'industry': ['publishing', 'social media', ],  #NOTE there's NO option for all here
    },
}

mock_u__ALL = [
    mock_u__Oliver,
    mock_u__Ryan,
]

#endregion endpoint /users


#region endpoint /groups

mock_g__Newyorkfashionclub = {
    'id'        : 1,
    'name'      : 'New York Fashion Club',
    'desc'      : 'New York Fashion Club brings together experts in fashion including stylists, designers, models, and producers. Join to stay uptodate with New York fashion',
    'moderator' : {'fullname': 'Alice Knight', 'TODO': 'user dict here eg id, avatar', },

    'connection_list' : [
        {'fullname': 'Jane Sloan', 'TODO': 'user dict here eg id, avatar', },
        {'fullname': 'Jane Sloan', 'TODO': 'user dict here eg id, avatar', },
    ],
}

mock_g__Stylebyscarlet = {
    'id'        : 2,
    'name'      : 'Styl by SCARLET',
    'desc'      : 'SCARLET is poised to be at the forefront of inclusive fashion and we need to band together to spread this agenda. Let’s find creative ways to advocate!',
    'moderator' : {'fullname': 'Adam Caprilio', 'TODO': 'user dict here eg id, avatar', },

    'connection_list' : [
        {'fullname': 'Jane Sloan', 'TODO': 'user dict here eg id, avatar', },
        {'fullname': 'Jane Sloan', 'TODO': 'user dict here eg id, avatar', },
    ],
}

mock_g__ALL = [
    mock_g__Newyorkfashionclub,
    mock_g__Stylebyscarlet,
]

#endregion endpoint /groups


#region endpoint /matchs

mock_m__Oliver = {
    'id'  : 1,
    'uid' : 1,

    'looking_for': [
        {
            'role'            : 'Property developer',
            'industry'        : 'Technology',
            'timeline'        : '3 months',
            'country'         : 'Singapore',
            'job_description' : 'Sell door lock tech solution',
        },
        {
            'role'            : 'fashion stylist',
            'industry'        : 'TV',
            'timeline'        : '3 months',
            'country'         : 'Singapore',
            'job_description' : 'Hire look on branch',
        },
    ],
}

mock_m__ALL = [
    mock_m__Oliver,
]

#endregion endpoint /matchs

#endregion mock result vs endpoint mapping


#region handle_request

def handle_request(req, resp)->'is handeld True/False':
    params = req.params
    n = params.get('n')  # n aka noun
    m = params.get('m')  # m aka method

    try:    handler_method = MOCK_ENDPOINT[n][m]
    except: handler_method = None

    if handler_method:
        handler_method(resp)
        return True
    else:
        return False


#region endpoint /users

def mock_users_GET(resp):
    resp.status = falcon.HTTP_200
    resp.body   = json.dumps(mock_u__ALL)


def mock_users_GET_atid_1(resp):
    resp.status = falcon.HTTP_200
    resp.body   = json.dumps(mock_u__Oliver)


def mock_users_GET_atid_2(resp):
    resp.status = falcon.HTTP_200
    resp.body   = json.dumps(mock_u__Ryan)


def mock_groups_GET_atid_1(resp):
    resp.status = falcon.HTTP_200
    resp.body   = json.dumps(mock_g__Newyorkfashionclub)

#endregion endpoint /users


#region endpoint /matchings

def mock_matchings_GET(resp):
    resp.status = falcon.HTTP_200
    resp.body   = json.dumps(mock_m__ALL)


def mock_matchings_GET_atid_1(resp):
    resp.status = falcon.HTTP_200
    resp.body   = json.dumps(mock_m__Oliver)

#endregion endpoint /matchings


MOCK_ENDPOINT = {

    #region /users
    'users': {
        'GET': mock_users_GET,
    },

    'users/1': {
        'GET': mock_users_GET_atid_1,
    },

    'users/2': {
        'GET': mock_users_GET_atid_2,
    },
    #endregion /users

    'groups/1': {
        'GET': mock_groups_GET_atid_1,
    },

    #region /matchings
    'matchings': {
        'GET': mock_matchings_GET,
    },

    'matchings/1': {
        'GET': mock_matchings_GET_atid_1,
    },
    #endregion /matchings

}

#endregion handle_request
