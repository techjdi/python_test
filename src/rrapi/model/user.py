from sqlalchemy import Column, Integer, Text

from src.rrapi.model import Base


class User(Base):
    __tablename__ = 'users'

    id                            = Column(Integer, primary_key = True)

    first_name                    = Column(Text,   nullable = False)
    last_name                     = Column(Text,   nullable = False)
    email                         = Column(Text,   nullable = False, unique = True)
    password                      = Column(Text,   nullable = False)
