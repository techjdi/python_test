from sqlalchemy import Column, Integer, Text

from src.rrapi.model import Base


class Group(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key = True)

    name = Column(Text,   nullable = False)
    #TODO storage structure for member
