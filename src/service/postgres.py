import os
from dotenv import load_dotenv


load_dotenv()  # load .env ref. https://pypi.org/project/python-dotenv/

DB_URL  = os.environ.get('DB_URL')
DB_USER = os.environ.get('DB_USER')
DB_PASS = os.environ.get('DB_PASS')
DB_HOST = os.environ.get('DB_HOST')
DB_PORT = os.environ.get('DB_PORT')
DB_NAME = os.environ.get('DB_NAME')
